import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { css } from 'emotion';

const style = {
  main: css`
    border: none;
    display: block;
    width: 100%;
    text-align: right;
    background: transparent;
    font-size: inherit;
  `,
};

class Input extends PureComponent {
  static propTypes = {
    onUpdate: PropTypes.func,
    value: PropTypes.number,
  };
  static defaultProps = {
    onUpdate: () => {},
    value: 0,
  };

  onChange = e => {
    const newValue = e.target.value.replace(/,/g, '.');
    if (!/^[0-9]+\.?([0-9]{1,2})?$/.test(newValue) && newValue !== '') return;
    this.props.onUpdate(Number(newValue));
  };

  render() {
    const { value } = this.props;
    return (
      <input
        ref={this.inputRef}
        className={style.main}
        type="number"
        onChange={this.onChange}
        value={String(value)}
      />
    );
  }
}

export default Input;
