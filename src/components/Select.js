import React from 'react';
import PropTypes from 'prop-types';
import { css } from 'emotion';

const style = {
  main: css`
    border: none;
    background: transparent;
    font-size: inherit;
  `,
};

const Select = ({ onUpdate, options, selected }) => (
  <select className={style.main} onChange={e => onUpdate(e.target.value)} value={selected}>
    {options.map(option => (
      <option key={`option.${option}`}>{option}</option>
    ))}
  </select>
);

Select.propTypes = {
  onUpdate: PropTypes.func,
  options: PropTypes.arrayOf(PropTypes.string),
};

Select.defaultProps = {
  onUpdate: () => {},
  options: [],
};

export default Select;
