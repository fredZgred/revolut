import React from 'react';
import PropTypes from 'prop-types';
import { css } from 'emotion';

const style = {
  main: css`
    margin: 0;
    padding: 0;
    font-size: inherit;
  `,
};

const Heading = ({ children }) => <h1 className={style.main}>{children}</h1>;

Heading.propTypes = {
  children: PropTypes.node,
};

export default Heading;
