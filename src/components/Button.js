import React from 'react';
import PropTypes from 'prop-types';
import { css, cx } from 'emotion';

const style = {
  main: css`
    border: none;
    font-size: inherit;
    margin: 0;
    overflow: visible;
    padding: 0 6px;
    color: inherit;
  `,
  block: css`
    display: flex;
    justify-content: center;
    padding: 10px;
    width: 100%;
  `,
  disabled: css`
    opacity: 0.5;
  `,
  size: size => css`
    height: ${{ normal: 30, large: 40 }[size]}px;
    min-width: ${{ normal: 30, large: 40 }[size]}px;
  `,
  color: color => css`
    background: ${color};
  `,
  border: color => css`
    border: 2px solid ${color};
  `,
  rounded: size => css`
    border-radius: ${{ normal: 15, large: 20 }[size]}px;
  `,
};

const Button = ({ children, onClick, block, disabled, size, color, border, rounded }) => (
  <button
    onClick={onClick}
    disabled={disabled}
    className={cx(style.main, style.size(size), style.color(color), {
      [style.block]: block,
      [style.disabled]: disabled,
      [style.border('lightgrey')]: Boolean(border),
      [style.rounded(size)]: rounded,
    })}
  >
    {children}
  </button>
);

Button.propTypes = {
  children: PropTypes.node,
  onClick: PropTypes.func,
  block: PropTypes.bool,
  disabled: PropTypes.bool,
  size: PropTypes.oneOf(['normal', 'large']),
  color: PropTypes.oneOf(['white', 'pink']),
  border: PropTypes.oneOf(['grey']),
  rounded: PropTypes.bool,
};

Button.defaultProps = {
  onClick: () => {},
  block: false,
  disabled: false,
  size: 'normal',
  color: 'white',
  rounded: false,
};

export default Button;
