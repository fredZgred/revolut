import React from 'react';
import { shallow, mount } from 'enzyme';
import Button from './Button';

describe('<Button />', () => {
  it('renders without crashing', () => {
    const wrapper = shallow(<Button />);
    expect(wrapper).toHaveLength(1);
  });
  it('renders children', () => {
    const wrapper = shallow(
      <Button>
        <span>content</span>
      </Button>,
    );
    expect(wrapper.contains(<span>content</span>)).toBeTruthy();
  });
  it('calls onClick callback on click event', () => {
    const onClick = jest.fn();
    const wrapper = shallow(<Button onClick={onClick} />);
    wrapper.simulate('click');
    expect(onClick).toHaveBeenCalled();
  });
  it('doent call onClick callback when is disabled', () => {
    const onClick = jest.fn();
    const wrapper = mount(<Button disabled onClick={onClick} />);
    wrapper.simulate('click');
    expect(onClick).not.toHaveBeenCalled();
  });
});
