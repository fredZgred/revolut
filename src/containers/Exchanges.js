import { connect } from 'react-redux';
import Exchanges from '../views/Exchanges';
import {
  amountChanged,
  currencyChanged,
  currenciesSwapRequested,
  exchangeRequested,
} from '../store/actions';

const mapState = ({
  sourceAccount,
  targetAccount,
  exchangeRate,
  currencies,
  exchangeButtonDisabled,
}) => ({
  sourceAccount,
  targetAccount,
  exchangeRate,
  currencies,
  exchangeButtonDisabled,
});

const mapDispatch = {
  onAmountChange: amountChanged,
  onCurrencyChange: currencyChanged,
  onCurrenciesSwapRequest: currenciesSwapRequested,
  onExchangeRequest: exchangeRequested,
};

export default connect(
  mapState,
  mapDispatch,
)(Exchanges);
