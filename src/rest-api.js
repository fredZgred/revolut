import { restServerUrl } from './config';

export const doExchange = ({ sourceCurrency, targetCurrency, amount }) =>
  fetch(`${restServerUrl}/exchange`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({
      sourceCurrency,
      targetCurrency,
      amount,
    }),
  });
