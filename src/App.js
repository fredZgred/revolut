import React, { PureComponent } from 'react';
import { Provider } from 'react-redux';
import createStore from './store';
import Exchanges from './containers/Exchanges';

class App extends PureComponent {
  store = createStore();

  render() {
    return (
      <Provider store={this.store}>
        <Exchanges />
      </Provider>
    );
  }
}

export default App;
