export const amountChanged = ({ kind, value }) => ({
  type: amountChanged.type,
  kind,
  value,
});
amountChanged.type = 'AMOUNT_CHANGED';

export const currencyChanged = ({ kind, value }) => ({
  type: currencyChanged.type,
  kind,
  value,
});
currencyChanged.type = 'CURRENCY_CHANGED';

export const currenciesSwapRequested = () => ({
  type: currenciesSwapRequested.type,
});
currenciesSwapRequested.type = 'CURRENCIES_SWAP_REQUEST';

export const exchangeRateChanged = ({ exchangeRate }) => ({
  type: exchangeRateChanged.type,
  exchangeRate,
});
exchangeRateChanged.type = 'EXCHANGE_RATE_CHANGED';

export const exchangeRequested = () => ({
  type: exchangeRequested.type,
});
exchangeRequested.type = 'EXCHANGE_REQUESTED';

export const exchangeFinished = () => ({
  type: exchangeFinished.type,
});
exchangeFinished.type = 'EXCHANGE_FINISHED';

export const balanceUpdated = ({ currency, amount }) => ({
  type: balanceUpdated.type,
  currency,
  amount,
});
balanceUpdated.type = 'BALANCE_UPDATED';
