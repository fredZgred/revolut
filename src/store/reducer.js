import {
  amountChanged,
  currencyChanged,
  currenciesSwapRequested,
  exchangeRateChanged,
  exchangeRequested,
  exchangeFinished,
  balanceUpdated,
} from './actions';
import { round } from '../utils';

const initialState = {
  balances: {
    EUR: 1000,
    USD: 50,
    GBP: 0,
  },
  exchangeInProgress: false,
  sourceAccount: {
    currency: 'EUR',
    balance: 1000,
    amount: 0,
  },
  targetAccount: {
    currency: 'USD',
    balance: 50,
    amount: 0,
  },
  exchangeRate: 0,
  currencies: ['EUR', 'USD', 'GBP'],
  exchangeButtonDisabled: true,
};

const swapCurrencies = state => ({
  ...state,
  sourceAccount: {
    ...state.targetAccount,
    amount: 0,
  },
  targetAccount: {
    ...state.sourceAccount,
    amount: 0,
  },
  exchangeRate: 0,
});

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case amountChanged.type: {
      const sourceAmount =
        action.kind === 'source' ? action.value : round(action.value / state.exchangeRate, 2);
      const targetAmount =
        action.kind === 'target' ? action.value : round(action.value * state.exchangeRate, 2);
      const exchangeButtonDisabled =
        state.exchangeInProgress ||
        sourceAmount === 0 ||
        sourceAmount > state.sourceAccount.balance;

      return {
        ...state,
        sourceAccount: {
          ...state.sourceAccount,
          amount: sourceAmount,
        },
        targetAccount: {
          ...state.targetAccount,
          amount: targetAmount,
        },
        exchangeButtonDisabled,
      };
    }

    case currencyChanged.type: {
      const accountKeyName = `${action.kind}Account`;
      const accountOppositeKeyName = `${action.kind === 'source' ? 'target' : 'source'}Account`;

      if (state[accountOppositeKeyName].currency === action.value) {
        return swapCurrencies(state);
      }
      return {
        ...state,
        [accountKeyName]: {
          currency: action.value,
          balance: state.balances[action.value],
          amount: 0,
        },
        exchangeRate: 0,
      };
    }

    case currenciesSwapRequested.type:
      return swapCurrencies(state);

    case exchangeRateChanged.type:
      return {
        ...state,
        exchangeRate: action.exchangeRate,
      };

    case exchangeRequested.type:
      return {
        ...state,
        exchangeInProgress: true,
        exchangeButtonDisabled: true,
      };

    case exchangeFinished.type:
      return {
        ...state,
        sourceAccount: {
          ...state.sourceAccount,
          amount: 0,
        },
        targetAccount: {
          ...state.targetAccount,
          amount: 0,
        },
        exchangeInProgress: false,
      };

    case balanceUpdated.type: {
      const { currency, amount } = action;
      return {
        ...state,
        balances: {
          ...state.balances,
          [currency]: amount,
        },
        sourceAccount: {
          ...state.sourceAccount,
          ...(state.sourceAccount.currency === currency ? { balance: amount } : {}),
        },
        targetAccount: {
          ...state.targetAccount,
          ...(state.targetAccount.currency === currency ? { balance: amount } : {}),
        },
      };
    }

    default:
      return state;
  }
};

export default reducer;
