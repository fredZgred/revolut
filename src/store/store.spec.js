import createStore from './';

const makeInitialState = part =>
  Object.assign(
    {
      balances: {},
      exchangeInProgress: false,
      sourceAccount: {},
      targetAccount: {},
      exchangeRate: 0,
      currencies: [],
      exchangeButtonDisabled: true,
    },
    part,
  );

describe('Store', () => {
  describe('during amount change', () => {
    it('calculates target amount on source amount change', () => {
      const store = createStore(
        makeInitialState({
          sourceAccount: {
            amount: 0,
          },
          targetAccount: {
            amount: 0,
          },
          exchangeRate: 0.5,
        }),
      );
      store.dispatch({ type: 'AMOUNT_CHANGED', kind: 'source', value: 10 });
      expect(store.getState().sourceAccount).toEqual({
        amount: 10,
      });
      expect(store.getState().targetAccount).toEqual({
        amount: 5,
      });
    });
    it('calculates source amount on target amount change', () => {
      const store = createStore(
        makeInitialState({
          sourceAccount: {
            amount: 0,
          },
          targetAccount: {
            amount: 0,
          },
          exchangeRate: 0.5,
        }),
      );
      store.dispatch({ type: 'AMOUNT_CHANGED', kind: 'target', value: 10 });
      expect(store.getState().sourceAccount).toEqual({
        amount: 20,
      });
      expect(store.getState().targetAccount).toEqual({
        amount: 10,
      });
    });
    it('disables exchange button when source amount is bigger than balance', () => {
      const store = createStore(
        makeInitialState({
          sourceAccount: {
            balance: 9,
            amount: 0,
          },
          targetAccount: {
            amount: 0,
          },
          exchangeRate: 0.5,
        }),
      );
      store.dispatch({ type: 'AMOUNT_CHANGED', kind: 'source', value: 10 });
      expect(store.getState()).toHaveProperty('exchangeButtonDisabled', true);
    });
    it('disables exchange button when source amount 0', () => {
      const store = createStore(
        makeInitialState({
          sourceAccount: {
            amount: 0,
          },
          targetAccount: {
            amount: 0,
          },
          exchangeRate: 0.5,
        }),
      );
      store.dispatch({ type: 'AMOUNT_CHANGED', kind: 'source', value: 0 });
      expect(store.getState()).toHaveProperty('exchangeButtonDisabled', true);
    });
    it('disables exchange button when there is an exchange in progress', () => {
      const store = createStore(
        makeInitialState({
          exchangeInProgress: true,
          sourceAccount: {
            amount: 0,
          },
          targetAccount: {
            amount: 0,
          },
          exchangeRate: 0.5,
        }),
      );
      store.dispatch({ type: 'AMOUNT_CHANGED', kind: 'source', value: 0 });
      expect(store.getState()).toHaveProperty('exchangeButtonDisabled', true);
    });
  });
  describe('during currency change', () => {
    it('sets properly account data', () => {
      const store = createStore(
        makeInitialState({
          balances: {
            USD: 999,
          },
          sourceAccount: {
            currency: 'PLN',
            balance: 123,
            amount: 12,
          },
        }),
      );
      store.dispatch({ type: 'CURRENCY_CHANGED', kind: 'source', value: 'USD' });
      expect(store.getState()).toHaveProperty('sourceAccount', {
        currency: 'USD',
        balance: 999,
        amount: 0,
      });
    });
    it('resets exchange rate', () => {
      const store = createStore(
        makeInitialState({
          balances: {
            USD: 999,
          },
          exchangeRate: 99,
        }),
      );
      store.dispatch({ type: 'CURRENCY_CHANGED', kind: 'source', value: 'USD' });
      expect(store.getState()).toHaveProperty('exchangeRate', 0);
    });
  });
});
