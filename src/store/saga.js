import { eventChannel } from 'redux-saga';
import { call, fork, takeEvery, put, select } from 'redux-saga/effects';
import { doExchange } from '../rest-api';
import { onExchangeRateChange, onBalanceChange, setCurrentCurrencies } from '../socket-api';
import {
  currencyChanged,
  currenciesSwapRequested,
  exchangeRateChanged,
  exchangeRequested,
  exchangeFinished,
  balanceUpdated,
} from './actions';

function* handleExchangeRateChange() {
  const exchangeRatesChannel = yield call(eventChannel, onExchangeRateChange);
  yield takeEvery(exchangeRatesChannel, function* updateRate(exchangeRate) {
    yield put(exchangeRateChanged({ exchangeRate }));
  });
}

function* handleBalanceChange() {
  const balanceChannel = yield call(eventChannel, onBalanceChange);
  yield takeEvery(balanceChannel, function* updateBalance({ currency, amount }) {
    yield put(balanceUpdated({ currency, amount }));
  });
}

function* setCurrenciesOnBackend() {
  const { sourceAccount, targetAccount } = yield select();
  yield call(setCurrentCurrencies, sourceAccount.currency, targetAccount.currency);
}

function* listenForCurrencyChange() {
  yield takeEvery([currencyChanged.type, currenciesSwapRequested.type], setCurrenciesOnBackend);
}

function* listenForExchangeAttempt() {
  yield takeEvery(exchangeRequested.type, function* fulfillExchangeRequest() {
    const { sourceAccount, targetAccount } = yield select();
    try {
      yield call(doExchange, {
        sourceCurrency: sourceAccount.currency,
        targetCurrency: targetAccount.currency,
        amount: sourceAccount.amount,
      });
      yield put(exchangeFinished());
    } catch (e) {
      // error handling here
    }
  });
}

function* rootSaga() {
  yield fork(handleExchangeRateChange);
  yield fork(handleBalanceChange);
  yield fork(setCurrenciesOnBackend);
  yield fork(listenForCurrencyChange);
  yield fork(listenForExchangeAttempt);
}

export default rootSaga;
