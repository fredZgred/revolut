import { websocketServerUrl } from './config';

let socket = null;
const messageHandlers = new Map();
const messagesBuffer = new Set();
const onSocketMessage = e => {
  const { type, data } = JSON.parse(e.data);
  const handler = messageHandlers.get(type);
  if (handler) {
    handler(data);
    return;
  }
  messagesBuffer.add({
    type,
    data,
  });
};
const socketReady = new Promise(resolve => {
  socket = new WebSocket(websocketServerUrl);
  socket.addEventListener('message', onSocketMessage);
  socket.addEventListener('open', () => {
    resolve();
  });
});
const addMessageHandler = (type, handler) => {
  messageHandlers.set(type, handler);
  messagesBuffer.forEach(bufferedMessage => {
    if (bufferedMessage.type === type) {
      handler(bufferedMessage.data);
      messagesBuffer.delete(bufferedMessage);
    }
  });
};
const removeMessageHandler = type => {
  messageHandlers.delete(type);
  if (!messageHandlers.size) {
    socket.removeEventListener('message', onSocketMessage);
    socket.close();
  }
};

export const onExchangeRateChange = onChange => {
  addMessageHandler('exchange-rate', onChange);
  return () => {
    removeMessageHandler('exchange-rate');
  };
};

export const onBalanceChange = onChange => {
  addMessageHandler('balance-update', onChange);
  return () => {
    removeMessageHandler('balance-update');
  };
};

export const setCurrentCurrencies = async (source, target) => {
  await socketReady;
  socket.send(
    JSON.stringify({
      type: 'currencies-changed',
      data: {
        source,
        target,
      },
    }),
  );
};
