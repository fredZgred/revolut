export const round = (float, positions) => {
  const factor = Math.pow(10, positions);
  return Math.round(float * factor) / factor;
};
