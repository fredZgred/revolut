import React from 'react';
import PropTypes from 'prop-types';
import { css } from 'emotion';
import { round } from '../utils';
import { Button, X, Heading, Input, Select } from '../components';

const style = {
  view: css`
    display: flex;
    flex-direction: column;
    height: 100vh;
    box-sizing: border-box;
  `,
  topBar: css`
    padding: 10px;
    display: flex;
    align-items: center;
  `,
  topBarTitle: css`
    flex-grow: 1;
  `,
  content: css`
    display: flex;
    flex-direction: column;
    flex-grow: 1;
  `,
  currencyArea: css`
    padding: 10px;
    display: flex;
    align-items: center;
    height: 100px;
    font-size: 24px;
  `,
  currencyAreaAmount: css`
    flex-grow: 1;
  `,
  currencyAreaBalance: css`
    height: 0;
    font-size: 12px;
    align-items: flex-end;
    white-space: nowrap;
    padding-left: 4px;
  `,
  middleButtons: css`
    display: flex;
    justify-content: space-between;
    align-items: center;
    height: 0;
    padding: 0 10px;
    z-index: 1;
  `,
  greyArea: css`
    display: flex;
    flex-direction: column;
    justify-content: space-between;
    flex-grow: 1;
    background: lightgrey;
  `,
  submit: css`
    padding: 20px;
  `,
};

const renderCurrencyArea = ({ account, currencies, onCurrencyChange, onAmountChange }) => (
  <div className={style.currencyArea}>
    <div>
      <Select options={currencies} selected={account.currency} onUpdate={onCurrencyChange} />
      <div className={style.currencyAreaBalance}>Balance: {account.balance}</div>
    </div>
    <div className={style.currencyAreaAmount}>
      <Input value={account.amount} onUpdate={onAmountChange} />
    </div>
  </div>
);

function Exchanges({
  sourceAccount,
  targetAccount,
  exchangeRate,
  currencies,
  exchangeButtonDisabled,
  onAmountChange,
  onCurrencyChange,
  onCurrenciesSwapRequest,
  onExchangeRequest,
}) {
  return (
    <div className={style.view}>
      <div className={style.topBar}>
        <Button>
          <X />
        </Button>
        <div className={style.topBarTitle}>
          <Heading>Exchange</Heading>
        </div>
        <Button>Auto A</Button>
      </div>
      {renderCurrencyArea({
        account: sourceAccount,
        currencies,
        onCurrencyChange: value => onCurrencyChange({ kind: 'source', value }),
        onAmountChange: value => onAmountChange({ kind: 'source', value }),
      })}
      <div className={style.middleButtons}>
        <Button rounded border={'grey'} onClick={onCurrenciesSwapRequest}>
          ^^
        </Button>
        <Button rounded border={'grey'}>
          1 {sourceAccount.currency} = {round(exchangeRate, 4)} {targetAccount.currency}
        </Button>
        <div />
      </div>
      <div className={style.greyArea}>
        {renderCurrencyArea({
          account: targetAccount,
          currencies,
          onCurrencyChange: value => onCurrencyChange({ kind: 'target', value }),
          onAmountChange: value => onAmountChange({ kind: 'target', value }),
        })}
        <div className={style.submit}>
          <Button
            block
            rounded
            size={'large'}
            color={'pink'}
            disabled={exchangeButtonDisabled}
            onClick={onExchangeRequest}
          >
            Exchange
          </Button>
        </div>
      </div>
    </div>
  );
}

const accountPropType = PropTypes.shape({
  currency: PropTypes.string,
  balance: PropTypes.number,
  amount: PropTypes.number,
});

Exchanges.propTypes = {
  sourceAccount: accountPropType,
  targetAccount: accountPropType,
  exchangeRate: PropTypes.number,
  currencies: PropTypes.arrayOf(PropTypes.string),
  onAmountChange: PropTypes.func,
  onCurrencyChange: PropTypes.func,
  onCurrenciesSwapRequest: PropTypes.func,
  onExchangeRequest: PropTypes.func,
};

Exchanges.defaultProps = {
  sourceAccount: {},
  targetAccount: {},
  exchangeRate: 0,
  currencies: [],
  onAmountChange: () => {},
  onCurrencyChange: () => {},
  onCurrenciesSwapRequest: () => {},
  onExchangeRequest: () => {},
};

export default Exchanges;
