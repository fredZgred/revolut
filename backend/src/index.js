const http = require('http');
const { initialize } = require('./exchange-rates');
const createRestServer = require('./rest');
const createSocketServer = require('./socket');

initialize();

const server = http.createServer();
createRestServer(server);
createSocketServer(server);

server.listen(8080, () => {
  console.info('Server listening on port 8080');
});
