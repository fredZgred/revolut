const { parse } = require('url');
const WS = require('ws');
const { onRatesUpdate, getRates } = require('./exchange-rates');
const { onWalletUpdate } = require('./user-context');

module.exports = server => {
  const socketServer = new WS.Server({ noServer: true });

  server.on('upgrade', (request, socket, head) => {
    const pathname = parse(request.url).pathname;

    if (pathname !== '/ws') {
      socket.destroy();
      return;
    }
    socketServer.handleUpgrade(request, socket, head, ws => {
      socketServer.emit('connection', ws, request);
    });
  });

  socketServer.on('connection', socket => {
    let unsubscribeFromRatesUpdates = () => {};
    const handleCurrenciesChange = async ({ source, target }) => {
      const handleNewRates = rates => {
        const data = rates.get(source).get(target);
        socket.send(
          JSON.stringify({
            type: 'exchange-rate',
            data,
          }),
        );
      };

      unsubscribeFromRatesUpdates();
      unsubscribeFromRatesUpdates = onRatesUpdate(handleNewRates);
      const rates = await getRates();
      handleNewRates(rates);
    };

    socket.on('message', rawMessage => {
      const { type, data } = JSON.parse(rawMessage);
      switch (type) {
        case 'currencies-changed':
          handleCurrenciesChange(data);
          break;

        default:
          console.error(`Invalid message ${type}`);
      }
    });

    onWalletUpdate(({ currency, amount }) => {
      socket.send(
        JSON.stringify({
          type: 'balance-update',
          data: { currency, amount },
        }),
      );
    });
  });
};
