const wallet = new Map([['EUR', 1000], ['USD', 50], ['GBP', 0]]);
const updateCallbacks = new Set();

exports.transaction = () => {
  const newWallet = new Map();
  return {
    setBalance(currency, amount) {
      newWallet.set(currency, amount);
      return this;
    },
    commit() {
      newWallet.forEach((amount, currency) => {
        wallet.set(currency, amount);
      });
      newWallet.forEach((amount, currency) => {
        updateCallbacks.forEach(callback => {
          callback({ currency, amount });
        });
      });
    },
  };
};
exports.getBalance = currency => wallet.get(currency);
exports.onWalletUpdate = callback => {
  updateCallbacks.add(callback);
  return () => {
    updateCallbacks.delete(callback);
  };
};
