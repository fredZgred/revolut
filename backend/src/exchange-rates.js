const http = require('http');

const supportedCurrencies = ['EUR', 'USD', 'GBP'];
const currentRates = new Map();
const ratesRefreshCallbacks = new Set();
let resolveInitialFetchIndicator = () => {};
const initialFetchFinished = new Promise(resolve => {
  resolveInitialFetchIndicator = resolve;
});

// because of free plan openexchangerates API replays with 403 for other currenciues than USD
// all usages of this variable are not considered part of implementation
const ignoreCurrencyForRequestAndUseUSD = true;

const getExchangeRates = currency =>
  new Promise((resolve, reject) => {
    const base = ignoreCurrencyForRequestAndUseUSD ? 'USD' : currency;
    http
      .request(
        {
          method: 'GET',
          host: 'openexchangerates.org',
          path: `/api/latest.json?base=${base}`,
          headers: {
            Authorization: 'Token 3a08a678dc5f483dbde5f6066f097970',
          },
        },
        response => {
          let body = '';
          response.on('data', chunk => {
            body += chunk;
          });
          response.on('end', () => {
            const response = JSON.parse(body);
            if (ignoreCurrencyForRequestAndUseUSD) {
              response.base = currency;
              const USDRate = 1 / response.rates[currency];
              response.rates.USD = USDRate;
              delete response.rates[currency];
            }
            resolve(response);
          });
        },
      )
      .on('error', e => {
        reject(e);
      })
      .end();
  });

const refreshRates = async () => {
  const responses = await Promise.all(supportedCurrencies.map(getExchangeRates));
  responses.forEach(({ base, rates }) => {
    const supportedRates = Object.entries(rates).filter(([currency]) =>
      supportedCurrencies.includes(currency),
    );

    const exchangeRates = new Map(supportedRates);
    currentRates.set(base, exchangeRates);
  });
};

const refreshAndNotify = async () => {
  await refreshRates();
  console.info('Exchange rates updated');
  ratesRefreshCallbacks.forEach(callback => {
    callback(currentRates);
  });
};

exports.initialize = () => {
  setInterval(refreshAndNotify, 10000);
  refreshAndNotify().then(() => {
    resolveInitialFetchIndicator();
  });
};

exports.onRatesUpdate = callback => {
  ratesRefreshCallbacks.add(callback);
  return () => {
    ratesRefreshCallbacks.delete(callback);
  };
};

exports.getRates = async () => {
  await initialFetchFinished;
  return currentRates;
};
