const express = require('express');
const cors = require('cors');
const morgan = require('morgan');
const { getBalance, transaction } = require('./user-context');
const { getRates } = require('./exchange-rates');

const round = (float, positions) => {
  const factor = Math.pow(10, positions);
  return Math.round(float * factor) / factor;
};

module.exports = server => {
  const app = express();
  server.on('request', app);

  app.use(express.json());
  app.use(cors());
  app.use(morgan('tiny'));

  app.post('/api/exchange', async (req, res) => {
    const { sourceCurrency, targetCurrency, amount } = req.body;

    if (getBalance(sourceCurrency) < amount) {
      res.sendStatus(405);
      return;
    }

    const rates = await getRates();
    const rate = rates.get(sourceCurrency).get(targetCurrency);
    const targetAmount = amount * rate;

    const initialSourceAmount = getBalance(sourceCurrency);
    const initialTargetAmount = getBalance(targetCurrency);
    const endSourceAmount = round(initialSourceAmount - amount, 2);
    const endTargetAmount = round(initialTargetAmount + targetAmount, 2);

    transaction()
      .setBalance(sourceCurrency, endSourceAmount)
      .setBalance(targetCurrency, endTargetAmount)
      .commit();

    res.sendStatus(200);
  });
};
