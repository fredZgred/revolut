# Revolut development task

You can find here two apps:
 - client (repository root)
 - backend (backend folder on the root)
 
Backend app abstracts integration with external services regarding exchange rates. It also manages the user balance data.

Client app focuses on ui aspect. It consumes balance and exchange rates data and renders the exchange widget.

Client app communicates with the backend app over websocket and rest api. Websocket communication is used mostly for getting updates on current exchange rate and balances. Rest api contains one endpoint for doing the actual exchange.

## Development process
1. Identify and implement atomic ui components such as button or input
2. Compose the view with ui components
3. Add centralized state and implement interactions
4. Implement exchange rates fetching on the backend side and consume the data in fronend app
5. Implement exchange functionality with user balances on backend side and use it in frontend app
6. Cover app with tests

## Install dependencies

```bash
yarn
cd backend
yarn
```

## Run application

### backend
```bash
cd backend
yarn start
```

### client
```bash
yarn start
```
